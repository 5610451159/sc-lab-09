package model;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company> {

	@Override
	public int compare(Company com1, Company com2) {
		// TODO Auto-generated method stub
		double c1 = com1.getIncome() ;
		double c2 = com2.getIncome() ;
		
		if (c1 < c2) {
			return -1 ;
		}
		
		else if (c1 > c2) {
			return 1 ;
		}
		
		else {
			return 0 ;
		}
	}

}
