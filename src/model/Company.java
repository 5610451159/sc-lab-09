package model;

import interfaces.Taxable;

public class Company implements Taxable {
	String name ;
	double income ;
	double expense ;
	double profit ;

	public Company(String name, double income, double expense){
		this.name = name ;
		this.income = income ;
		this.expense = expense ;
		this.profit = income - expense ;
	}
	
	public String toString() {
		return "Name : " + name + " => income is " + income + " bath. " + " expense is " + expense + " bath. " + " profit is " + profit + " bath." ;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double tax = 0.0 ;
		
		tax = (income - expense) * 0.3 ;
		
		return tax ;
	}
	
	public double getIncome() {
		return income ;
	}
	
	public double getExpense() {
		return expense ;
	}
	
	public double getProfit() {
		return profit ;
	}
}
