package model;

import interfaces.Taxable;

public class Product implements Taxable, Comparable<Product> {
	String name ;
	double price ;
	
	public Product(String name, double price){
		this.name = name ;
		this.price = price ;
	}
	
	public String toString() {
		return "Name : " + name + " => price is " + price ;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double tax = 0.0 ;
		
		tax = price * 0.07 ;
		
		return tax ;
	}

	@Override
	public int compareTo(Product other) {
		// TODO Auto-generated method stub
		if (this.price < other.price) {
			return -1 ;
		}
		
		else if (this.price < other.price) {
			return 1 ;
		}
		
		else {
			return 0;
		}
	}

}
