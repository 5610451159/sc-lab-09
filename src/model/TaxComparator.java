package model;

import interfaces.Taxable;

import java.util.Comparator;

public class TaxComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		double oj1 = ((Taxable)o1).getTax() ;
		double oj2 = ((Taxable)o1).getTax() ;
		
		if (oj1 < oj2) {
			return -1 ;
		}
		
		else if (oj1 > oj2) {
			return 1 ;
		}
		
		else {
			return 0 ;
		}
	}

}
