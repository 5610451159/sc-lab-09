package control;

import java.util.ArrayList;
import java.util.Collections;

import model.Company;
import model.EarningComparator;
import model.ExpenseComparator;
import model.Person;
import model.Product;
import model.ProfitComparator;
import model.TaxComparator;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test test = new Test() ;
		
		test.testPerson() ;
		System.out.println("\n") ;
		test.testProduct() ;
		System.out.println("\n") ;
		test.testCompany() ;
		System.out.println("\n") ;
		test.testTax() ;
		
	}
	
	public void testPerson() {
		ArrayList<Person> person = new ArrayList<Person>() ;
		person.add(new Person("Noey", 154, 35000)) ;
		person.add(new Person("Papa", 167, 50000)) ;
		person.add(new Person("Mama", 157, 30000)) ;
		
		System.out.println("Test Person") ;
		Collections.sort(person) ;
		for (Person p : person) {
			System.out.println(p) ;
		}
	}
	
	public void testProduct() {
		ArrayList<Product> product = new ArrayList<Product>() ;
		product.add(new Product("P1", 1000)) ;
		product.add(new Product("P2", 500)) ;
		product.add(new Product("P3", 100)) ;
		
		System.out.println("Test Product") ;
		Collections.sort(product) ;
		for (Product p : product) {
			System.out.println(p) ;
		}
	}
	
	public void testCompany() {
		ArrayList<Company> company = new ArrayList<Company>() ;
		company.add(new Company("KFC", 50000, 5000)) ;
		company.add(new Company("Oishi", 500000, 150000)) ;
		company.add(new Company("Shabu", 100000, 55000)) ;
	
		System.out.println("Test Company") ;
		System.out.println("Income ::") ;
		Collections.sort(company, new EarningComparator()) ;
		for (Company c : company) {
			System.out.println(c) ;
		}
		
		System.out.println("Expense ::") ;
		Collections.sort(company, new ExpenseComparator()) ;
		for (Company c : company) {
			System.out.println(c) ;
		}
		
		System.out.println("Profit ::") ;
		Collections.sort(company, new ProfitComparator()) ;
		for (Company c : company) {
			System.out.println(c) ;
		}
	}
	
	public void testTax() {
		ArrayList<Object> oj = new ArrayList<Object>() ;
		
		Person person = new Person("Noey", 154, 35000) ;
		Product product = new Product("P1", 1000) ;
		Company company = new Company("KFC", 50000, 5000) ;
		
		oj.add(person) ;
		oj.add(product) ;
		oj.add(company) ;
		
		System.out.println("Test Tax") ;
		System.out.println("Noey : tax is " + person.getTax()) ;
		System.out.println("P1 : tax is " + product.getTax()) ;
		System.out.println("KFC : tax is " + company.getTax()) ;
		
		System.out.println("Tax Sort ::") ;
		Collections.sort(oj, new TaxComparator()) ;
		for (Object o : oj) {
			System.out.println(o) ;
		}
	}
}
